((Drupal) => {
  'use strict';

  Drupal.behaviors.growing_textareas = {
    attach: function (context, settings) {
      // forEach method, could be shipped as part of an Object Literal/Module
      var forEach = function (array, callback, scope) {
        for (var i = 0; i < array.length; i++) {
          callback.call(scope, i, array[i]); // passes back stuff we need
        }
      };

      var $textareas = document.querySelectorAll('.form-item--textarea textarea:not(.js-processed)');
      forEach($textareas, function (index, $textarea) {
        $textarea.classList.add('js-processed');
        autosize($textarea);
      });
    }
  };
})(Drupal);
